public class Main {
    public static void main(String[] args){
        // Using a command line display class that implements the UIViewInterface interface.
        // Owing to inheritance and polymorphism, any class that implements UIViewInterface can be instantiated here to derive another type of View - for eg: GUI
        // The type of display will be common throughout the class, so to ensure this, will pass this as an argument to the GameController constructor
        UIViewInterface ui = new CLIView();

        String gameIntro = "Welcome to Connect 4\n" +
                "There are 2 players: red and yellow\n" +
                "Player 1 is Red, Player 2 is Yellow\n" +
                "To play the game, type in the number of the column you want to drop you counter in\n" +
                "A player wins by connecting 4 counters in a row - vertically, horizontally or diagonally\n";

        ui.displayText(gameIntro);

        new GameController(ui);
    }
}
