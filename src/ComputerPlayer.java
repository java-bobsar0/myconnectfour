import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Class representing a computer player
 */
public class ComputerPlayer extends Player {
    Random random;

    /**
     * Creates a computer player object passing in "Computer" as the name of the player
     * @param token the token representation of the computer player as a {@code char}
     */
    public ComputerPlayer(char token) {
        super("Computer", token);
        // instantiate random field for obtaining random values
        random = new Random();
    }

    /**
     * Retrieves a random integer between 1 and the limit representing Computer player's input
     * @param limit max input value required
     * @return move as an {@code int}
     */
    @Override
    public int getMove(int limit) {
        try {
            // sleep for 1.5 sec to simulate the computer thinking
            TimeUnit.MILLISECONDS.sleep(1500);
        } catch (InterruptedException ignored){}

        int number = random.nextInt(limit);
        // add 1 to number so we don't get a zero value and so we can get the limit because limit is not exclusively returned by nextInt()
        return number + 1;
    }
}
