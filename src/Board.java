/**
 * Board class used to create the board object the game is played on and implement its associated behaviours
 */
public class Board {
    private final char[][] board;
    private final int horizontalSize;
    private final int verticalSize;

    /**
     * Creates a new Board object with defined vertical and horizontal sizes
     *
     * @param verticalSize   the vertical or y-axis board size
     * @param horizontalSize the horizontal or x-axis board size
     */
    public Board(int verticalSize, int horizontalSize) {
        this.verticalSize = verticalSize;
        this.horizontalSize = horizontalSize;
        board = new char[verticalSize][horizontalSize];
    }

    /**
     * Retrieves the horizontal size of the board
     *
     * @return horizontal size as an {@code int}
     */
    public int getHorizontalSize() {
        return horizontalSize;
    }

    /**
     * Returns a {@code String} representation of {@code Board} object
     * 'r' represents red tokenChar
     * 'y' represents yellow tokenChar
     */
    public String toString() {
        StringBuilder s = new StringBuilder();

        for (int i = 0; i < verticalSize; i++) {
            for (int j = 0; j < horizontalSize; j++) {
                if (board[i][j] == 'r') {
                    s.append("| " + "r" + " ");
                } else if (board[i][j] == 'y') {
                    s.append("| " + "y" + " ");
                } else {
                    s.append("|   ");
                }
            }
            s.append("|").append("\n");
        }
        s.append("  ");
        // prints numbers on the horizontal axis corresponding to each board column
        for (int i = 1; i <= horizontalSize; i++) {
            s.append(i).append("   ");
        }
        s.append("\n");
        return s.toString();
    }

    /**
     * Places tokenChar on the defined position on the board
     *
     * @param tokenChar  the token to be placed.
     * @param position the position on the horizontal axis to place the tokenChar in.
     * @return true or false representing whether the tokenChar is placed successfully or not
     */
    public boolean placeCounter(char tokenChar, int position) {
        for (int i = verticalSize - 1; i >= 0; i--) {
            if (board[i][position] != 'y' && board[i][position] != 'r') {
                board[i][position] = tokenChar;

                return true;
            }
        }
        return false;
    }

    /**
     * Checks horizontal, vertical and diagonal directions to determine if winner is found
     * Winner is determined when 4 counters of same color are connected in a row in either horizontal, vertical or diagonal direction
     *
     * @param tokenChar the token to be checked
     * @return true or false representing if a winner is found or not
     */
    public boolean isWinnerFound(char tokenChar) {
        return checkHorizontalWin(tokenChar) ||
                checkVerticalWin(tokenChar) ||
                checkRightDiagonalWin(tokenChar) ||
                checkLeftDiagonalWin(tokenChar);
    }

    /**
     * Checks if the game is a draw by traversing through the board to determine if all positions have been filled
     *
     * @return true or false representing if the game is a draw or not
     */
    public boolean isDrawFound() {
        for (int i = 0; i < verticalSize; i++) {
            for (int j = 0; j < horizontalSize; j++) {
                // if position is empty, it's not a draw so return immediately.
                // int 0 or char '\u0000' is default value of empty char position
                if (board[i][j] == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    // Checks if the tokenChar is connected 4 times in a row in the horizontal direction
    private boolean checkHorizontalWin(char tokenChar) {
        int count = 0;
        for (int i = verticalSize - 1; i >= 0; i--) {
            for (int j = 0; j < horizontalSize; j++) {
                if (board[i][j] == tokenChar) {
                    count++;
                    if (hasWon(count)) return true;
                } else {
                    count = 0;
                }
            }
            // reset count if we leave the current board level
            count = 0;
        }
        return false;
    }

    // Checks if the tokenChar is connected 4 times in a row in the vertical direction
    private boolean checkVerticalWin(char tokenChar) {
        int count = 0;
        for (int j = 0; j < horizontalSize; j++) {
            for (int i = verticalSize - 1; i >= 0; i--) {
                if (board[i][j] == tokenChar) {
                    count++;
                    if (hasWon(count)) return true;
                } else {
                    // reset count if no tokenChar in successive position in vertical axis was found
                    count = 0;
                }
            }
            count = 0;
        }
        return false;
    }

    // Starts from the bottom and checks if the tokenChar is connected 4 times in a row in the right diagonal/step-wise direction
    private boolean checkRightDiagonalWin(char tokenChar) {
        int count = 0;
        for (int i = verticalSize - 1; i >= 0; i--) {
            for (int j = 0; j < horizontalSize - 1; j++) {
                if (hasWonDiagonally(tokenChar, count, i, j, Diagonal.RIGHT)) {
                    return true;
                }
                // reset count if tokenChar is not found in upper right diagonal
                count = 0;
            }
        }
        return false;
    }

    // Starts from the bottom and checks if the tokenChar is connected 4 times in a row in the left diagonal/step-wise direction
    private boolean checkLeftDiagonalWin(char tokenChar) {
        int count = 0;
        for (int i = verticalSize - 1; i >= 0; i--) {
            for (int j = horizontalSize - 1; j >= 0; j--) {
                if (hasWonDiagonally(tokenChar, count, i, j, Diagonal.LEFT)) {
                    return true;
                }
                // reset count if no corresponding upper left diagonal was found
                count = 0;
            }
        }
        return false;
    }

    // Helper function to check if user has won in the passed-in diagonal direction
    // Enum is used here so we don't have to explicitly pass in string values for the check. Less error-prone
    private boolean hasWonDiagonally(char tokenChar, int count, int i, int j, Diagonal direction) {
        // x and y are introduced so as not to overwrite i and j values
        int x = i;
        int y = j;
        // we check for y here as y is incremented within loop
        while (y < horizontalSize && board[x][y] == tokenChar) {
            count++;
            if (hasWon(count)) return true;

            if (direction == Diagonal.LEFT) {
                // decrement x and y to immediately check if there's a match with the tokenChar in the upper left diagonal
                x--;
                y--;
            } else if (direction == Diagonal.RIGHT) {
                // decrement x and increment y to immediately check if there's a match with the tokenChar in the upper right diagonal
                x--;
                y++;
            }
            // edge of the board has been reached so no need to check further
            if (x < 0 || y < 0) break;
        }
        return false;
    }

    // According to standard game, user wins if tokenChar is connected 4 rows in succession
    private boolean hasWon(int count) {
        return count >= 4;
    }

    // Enum representing right and left diagonal direction values
    private enum Diagonal {RIGHT, LEFT}
}