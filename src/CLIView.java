/**
 * Command-line interface implementation of the ViewInterface
 * This is used to display the game on the command line
 */
public class CLIView implements UIViewInterface {
    /**
     * Prints text to the console using system output stream.
     *
     * @param text text to display as a {@code String}
     */
    @Override
    public void displayText(String text) {
        System.out.println(text);
    }

    /**
     * Prints error message to the console using system error output stream
     *
     * @param message text to display as a {@code String}
     */
    @Override
    public void displayErrorMessage(String message) {
        System.err.println(message);
    }
}
