import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for controlling the actual game play
 */
class GameController {
    private final UIViewInterface ui;

    private final Board board;
    private final List<Player> players;
    private int currentPlayerIndex;

    /**
     * Instantiates a new GameController game
     * @param ui the view implementation that renders data on screen. Interface is passed in to allow any of its implementations to be easily swapped
     */
    public GameController(UIViewInterface ui){
        board = new Board(6, 7);
        this.ui = ui;
        players = new ArrayList<>();
        currentPlayerIndex = 0;

        selectPlayers();
        playGame();
    }

    /*
    Select players following the requirements of 1 Human vs 1 Computer player
     */
    private void selectPlayers() {
        players.add(new HumanPlayer("Player 1", 'r'));
        players.add(new ComputerPlayer('y'));
    }

    private void playGame(){
        ui.displayText(board.toString());

        Player currentPlayer;
        boolean winnerFound = false;
        do{
            currentPlayer = getCurrentPlayer();
            ui.displayText(currentPlayer.getName() +" please enter a position:");
            try {
                int move = currentPlayer.getMove(board.getHorizontalSize());
                ui.displayText("Position " + move + " entered\n");

                // Add -1 to move to account for the fact that array index starts at 0 but board starts at 1
                if (!board.placeCounter(currentPlayer.getTokenChar(), move - 1)) {
                    ui.displayText("Your counter was not placed as column " + move + " is filled. Please try again");
                    continue;
                }
                ui.displayText(board.toString());

                winnerFound = board.isWinnerFound(currentPlayer.getTokenChar());
                currentPlayerIndex++;
            } catch (InvalidMoveException e) {
                ui.displayErrorMessage(e.getMessage());
            }
        } while(!winnerFound && !board.isDrawFound());

        // Print a winning message if winner is found, otherwise print a draw message as that is the only other condition via which loop above can be exited
        String message = winnerFound ? currentPlayer.getName() + " Wins!!!" : "Game is a Draw!";
        ui.displayText(message);

        askToPlayAgain();
    }

    private Player getCurrentPlayer(){
        // wrap around - reset index ot 0 if max index is reached
        if (currentPlayerIndex >= players.size()) {
            currentPlayerIndex = 0;
        }
        return players.get(currentPlayerIndex);
    }

    /*
   Handle play again feature if user wishes to immediately play the game again after gameplay has ended.
   This aims for a better user experience by avoiding that delay between stopping and restarting the app if user wishes to continue playing.
    */
    private void askToPlayAgain() {
        ui.displayText("\nPlay again? Enter Y for Yes or any other key to quit game.");

        // Use try-with-resources syntax to auto-close input reader after use
        try(BufferedReader input = new BufferedReader(new InputStreamReader(System.in))) {
            String line = input.readLine();

            if (line.equals("Y") || line.equals("y")) {
                new GameController(ui);
            } else {
                ui.displayText("Thank you for playing! Goodbye.");
            }
        } catch (IOException e) {
            ui.displayErrorMessage("An error occurred while trying to read your input. Please restart the game");
            // Exit the app gracefully
            System.exit(1);
        }
    }
}