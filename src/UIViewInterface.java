/**
 * Interface responsible for displaying data on the screen
 * Any class wishing to render data on the screen must implement all methods defined by this class
 */
public interface UIViewInterface {
    /**
     * Renders text to the screen
     *
     * @param text text to display as a {@code String}
     */
    void displayText(String text);

    /**
     * Renders error message to the screen
     *
     * @param msg message to display as a {@code String}
     */
    void displayErrorMessage(String msg);
}
