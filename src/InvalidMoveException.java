/**
 * Exception thrown at runtime when an invalid move is obtained from players
 */
class InvalidMoveException extends RuntimeException {
    public InvalidMoveException(String message) {
        super(message);
    }
}