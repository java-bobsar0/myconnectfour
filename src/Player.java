///**
// * Abstract Class representing the users of the game
// */
//public abstract class Player {
//    private final String name;
//    private final char counterChar;
//
//    /**
//     * Creates a new Player object for the game
//     * @param name  name of the player
//     * @param token the {@code char} counter that belongs to this player
//     */
//    public Player(String name, char token){
//        this.name = name;
//        this.counterChar = token;
//    }
//
//    public String getName() {
//        return name+"[" + counterChar + "]";
//    }
//
//    public char getCounterChar() {
//        return counterChar;
//    }
//
//    /**
//     * Retrieves the integer value of user input. This must be implemented by all sub classes
//     * @param limit max input value required
//     * @return user input as an {@code int}
//     */
//    public abstract int getMove(int limit);
//}

/**
 * Abstract Class representing the users of the game
 */
public abstract class Player {
    private final String name;
    private final char tokenChar;

    /**
     * Creates a new Player object for the game
     * @param name  name of the player
     * @param token the {@code char} counter that belongs to this player
     */
    // Tempted to pass Token object here but just passing token char to prevent tight coupling
    public Player(String name, char token){
        this.name = name;
        this.tokenChar = token;
    }

    public String getName() {
        return name+"[" + tokenChar + "]";
    }

    public char getTokenChar() {
        return tokenChar;
    }

    /**
     * Retrieves the integer value of user input.
     * @param limit max input value required
     * @return user input as an {@code int}
     */
    public abstract int getMove(int limit);
}

