import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class responsible for behaviours of a local human player
 * This inherits all fields of {@code Player} class and overrides abstract methods
 */
public class HumanPlayer extends Player {
    private final BufferedReader input;

    /**
     * Creates a local player object and takes in a {@code UIViewInterface} object to be able to flexibly print messages depending on the display
     * @param name  player name
     * @param token the {@code Token} object that belongs to this player
     */
    public HumanPlayer(String name, char token) {
        super(name, token);
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Retrieves integer input from local user.
     * If input is invalid, requests user to try again
     * @param inputLimit the maximum input value expected
     * @return the user input as an {@code int}
     */
    @Override
    public int getMove(int inputLimit){
        try {
            String line = input.readLine();
            int move = Integer.parseInt(line);
            // if move is a valid integer within the defined boundaries => 1 to maxLimit
            if(move >= 1 && move <= inputLimit) {
                return move;
            }
            throw new InvalidMoveException("Please enter an integer between 1 and " + inputLimit);

        } catch (NumberFormatException e) {
            throw new InvalidMoveException("Your input must be an integer between 1 and " + inputLimit + ". Please try again");
        } catch (IOException e) {
            throw new InvalidMoveException("An error occurred while trying to read your input. If error persists, please restart the game");
        }
    }
}
